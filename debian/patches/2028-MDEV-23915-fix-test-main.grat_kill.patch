Forwarded: https://github.com/MariaDB/server/pull/2028
Origin: https://patch-diff.githubusercontent.com/raw/MariaDB/server/pull/2028.patch
From: Daniel Black <daniel@mariadb.org>
Date: Wed, 23 Feb 2022 10:10:01 +1100
Subject: [PATCH] MDEV-23915 ER_KILL_DENIED_ERROR not passed a thread id

The 10.5 test error main.grant_kill showed up a incorrect
thread id on a big endian architecture.

The cause of this is the sql_kill_user function assumed the
error was ER_OUT_OF_RESOURCES, when the the actual error was
ER_KILL_DENIED_ERROR. ER_KILL_DENIED_ERROR as an error message
requires a thread id to be passed as unsigned long, however a
user/host was passed.

ER_OUT_OF_RESOURCES doesn't even take a user/host, despite
the optimistic comment. We remove this being passed as an
argument to the function so that when MDEV-21978 is implemented
one less compiler format warning is generated (which would
have caught this error sooner).

Thanks Otto for reporting and Marko for analysis.
---
 .../suite/galera/r/galera_kill_applier.result  |  4 ++++
 .../suite/galera/t/galera_kill_applier.test    |  6 ++++--
 sql/sql_parse.cc                               | 18 ++++++++++--------
 3 files changed, 18 insertions(+), 10 deletions(-)

--- a/mysql-test/suite/galera/r/galera_kill_applier.result
+++ b/mysql-test/suite/galera/r/galera_kill_applier.result
@@ -5,9 +5,13 @@ SELECT @@wsrep_slave_threads;
 @@wsrep_slave_threads
 1
 SET GLOBAL wsrep_slave_threads=2;
+KILL ID;
 Got one of the listed errors
+KILL QUERY ID;
 Got one of the listed errors
+KILL ID;
 Got one of the listed errors
+KILL QUERY ID;
 Got one of the listed errors
 SET GLOBAL wsrep_slave_threads=DEFAULT;
 connection node_1;
--- a/mysql-test/suite/galera/t/galera_kill_applier.test
+++ b/mysql-test/suite/galera/t/galera_kill_applier.test
@@ -16,21 +16,23 @@ SET GLOBAL wsrep_slave_threads=2;
 
 --let $applier_thread = `SELECT ID FROM INFORMATION_SCHEMA.PROCESSLIST WHERE USER = 'system user' AND STATE = 'wsrep applier idle' LIMIT 1`
 
---disable_query_log
+--replace_result $applier_thread ID
 --error ER_KILL_DENIED_ERROR,ER_KILL_DENIED_ERROR
 --eval KILL $applier_thread
 
+--replace_result $applier_thread ID
 --error ER_KILL_DENIED_ERROR,ER_KILL_DENIED_ERROR
 --eval KILL QUERY $applier_thread
 
 --let $aborter_thread = `SELECT ID FROM INFORMATION_SCHEMA.PROCESSLIST WHERE USER = 'system user' AND STATE = 'wsrep aborter idle' LIMIT 1`
 
+--replace_result $aborter_thread ID
 --error ER_KILL_DENIED_ERROR,ER_KILL_DENIED_ERROR
 --eval KILL $aborter_thread
 
+--replace_result $aborter_thread ID
 --error ER_KILL_DENIED_ERROR,ER_KILL_DENIED_ERROR
 --eval KILL QUERY $aborter_thread
---enable_query_log
 
 SET GLOBAL wsrep_slave_threads=DEFAULT;
 
--- a/sql/sql_parse.cc
+++ b/sql/sql_parse.cc
@@ -9393,15 +9393,18 @@ sql_kill_user(THD *thd, LEX_USER *user,
 {
   uint error;
   ha_rows rows;
-  if (likely(!(error= kill_threads_for_user(thd, user, state, &rows))))
-    my_ok(thd, rows);
-  else
+
+  switch (error= kill_threads_for_user(thd, user, state, &rows))
   {
-    /*
-      This is probably ER_OUT_OF_RESOURCES, but in the future we may
-      want to write the name of the user we tried to kill
-    */
-    my_error(error, MYF(0), user->host.str, user->user.str);
+  case 0:
+    my_ok(thd, rows);
+    break;
+  case ER_KILL_DENIED_ERROR:
+    my_error(error, MYF(0), (unsigned long) thd->thread_id);
+    break;
+  case ER_OUT_OF_RESOURCES:
+  default:
+    my_error(error, MYF(0));
   }
 }
 
